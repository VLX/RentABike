<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bare - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="css2/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <style>
    body {
        padding-top: 70px;
        /* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */
         }
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Home</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="signupf.php">Account</a>
                    </li>
                    <li>
                        <a href="moto.php">Rent</a>
                    </li>
                    <li>
                        <a href="news.php">News</a>
                    </li>
					<li>
                        <a href="loginform.php">Login</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">
  
        <div class="row">
		 <form role="form" name="request_form" id="request_form" method="post" action="signin.php" enctype="multipart/form-data" onsubmit="return validateForm()">
            <div class="col-lg-12 text-center" action="signup.php">
                 <div class="form-group">
				    <label >Login</label>
                 
                    
                    
				   <?php 
					
					//USE CASE A: Step 1 - Retrieve from db
				/*	try {
						$conn = new PDO("mysql:host=".$servername.";dbname=".$dbname.";charset=utf8" , $dbadmin_name, $dbadmin_password);
						// set the PDO error mode to exception
						$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

						$stmt = $conn->prepare("SELECT tid, title FROM tours where statusId=1"); 
						$stmt->execute();
					   //USE CASE A: Step 2 - Print output
						 foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $row) {
							echo "<option value='". $row["tid"] ."'>" . $row["title"] . "</option>";
						}
					}
					catch(PDOException $e)
					{
						$error = "Προέκυψε πρόβλημα με τη βάση δεδομένων!";
						echo "Connection failed: " . $e->getMessage();
					}
				  */ ?>
                   
            </div>
			   
			
            <form class="form-inline">
    <div class="form-group">
    <label for="username">UserName</label>
    <input type="text" class="form-control" id="exampleInputName2" placeholder="UserName" name="username">
  </div>
  <div class="form-group">
    <label for="password">Password</label>
    <input type="password" class="form-control" id="password" placeholder="password" name="password">
  </div>
  <button type="submit" class="btn btn-default">Login</button>
</form> 
			
            </div>
			
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- jQuery Version 1.11.1 -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
